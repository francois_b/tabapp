module.exports = {
  port: process.env.PORT || 8081,
  db: {
    database: process.env.DB_NAME || 'tabapp',
    user: process.env.DB_USER || 'tabapp',
    password: process.env.DB_PASS || 'tabapp',
    options: {
      dialect: process.env.DIALECT || 'sqlite',
      host: process.env.HOST || 'localhost',
      storage: './tabapp.sqlite'
    }
  }
}
